#!/bin/bash

export DB_DRIVER=org.postgresql.Driver
export DB_URL=jdbc:postgresql://localhost:5432/postgres
export DB_SCHEMA=parking
export DB_USER=postgres
export APP_PORT=9010
export DB_POOL_SIZE=10
export DB_QUERY_TIMEOUT=20000
export DB_DATABASE=postgres
export DB_PASSWORD=test1234
export DB_HOST=localhost

cd ..
gradle shadowJar

cd api-service
java -Xms256m -Xmx512m -Duser.timezone=UTC -Dvertx.logger-delegate-factory-class-name=io.vertx.core.logging.SLF4JLogDelegateFactory -Dlogback.configurationFile=src/main/resources/logback.xml -jar build/libs/parking-online-fat.jar

#java -Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=8000 -jar build/libs/parking-online-fat.jar
