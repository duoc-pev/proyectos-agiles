package cl.online.parking.util;

import gherkin.deps.com.google.gson.Gson;
import gherkin.deps.com.google.gson.GsonBuilder;
import io.vertx.core.json.JsonArray;
import org.apache.commons.lang3.StringUtils;

import java.time.Instant;

public class JsonUtil {

    private JsonUtil() {
        throw new IllegalAccessError(JsonUtil.class.toString());
    }
    public static final Gson GSON;

    static {
        GsonBuilder gsonBuilder = new GsonBuilder();
        GSON = gsonBuilder.create();
    }

    public static void addParam(JsonArray params, String value) {
        if (StringUtils.isNotEmpty(value)) {
            params.add(value);
        } else {
            params.addNull();
        }

    }

    public static void addParam(JsonArray params, Boolean value) {
        if (value != null) {
            params.add(value);
        } else {
            params.addNull();
        }

    }

    public static void addParam(JsonArray params, Instant value) {
        if (value != null) {
            params.add(value);
        } else {
            params.addNull();
        }

    }

    public static void addParam(JsonArray params, Double value) {
        if (value != null) {
            params.add(value);
        } else {
            params.addNull();
        }
    }

    public static void addParam(JsonArray params, Long value) {
        if (value != null) {
            params.add(value);
        } else {
            params.addNull();
        }
    }

    public static void addParam(JsonArray params, Integer value) {
        if (value != null) {
            params.add(value);
        } else {
            params.addNull();
        }
    }
}
