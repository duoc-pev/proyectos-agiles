package cl.online.parking.util;

import cl.online.parking.model.commons.TimeInstant;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class InstantUtil {

    private static final String FORMAT_INSTANT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FORMAT_INSTANT).withZone(ZoneOffset.UTC);

    private InstantUtil() {
        throw new IllegalAccessError(InstantUtil.class.toString());
    }




    public static Instant addDays(Instant dt, Long days) {
        return dt != null && days != null ? dt.plus(days, ChronoUnit.DAYS) : null;
    }


    /**
     * @return
     */
    public static String getStrActualTime() {
        return formatter.format(Instant.now());
    }

    public static Boolean isNotNullTimeInstant(TimeInstant timeInstant) {
        return (timeInstant != null && timeInstant.getTime() != null);
    }


}
