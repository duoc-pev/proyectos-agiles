package cl.online.parking.verticles;

import cl.online.parking.dao.JdbcRepositoryWrapper;
import cl.online.parking.service.business.RentService;
import cl.online.parking.service.business.RentServiceImpl;
import cl.online.parking.service.business.UserService;
import cl.online.parking.service.crud.RentServiceCRUD;
import cl.online.parking.service.crud.RentServiceCRUDImpl;
import cl.online.parking.service.crud.UserServiceCRUD;
import cl.online.parking.service.crud.UserServiceCRUDImpl;
import cl.online.parking.service.business.UserServiceImpl;
import cl.online.parking.util.AppEnum;
import cl.online.parking.util.SystemUtil;
import cl.online.parking.verticles.api.RentVerticle;
import cl.online.parking.verticles.api.UserVerticle;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

public class ApiMainVerticle extends RestAPIVerticle {

    public static final String API_ONLINE = "/api/online";
    private static final String API_HEALTHCHECK = API_ONLINE + "/healthcheck";

    // Services
    private UserServiceCRUD userServiceCRUD;
    private UserService userService;
    private RentServiceCRUD rentServiceCRUD;
    private RentService rentService;

    @Override
    public void start() throws Exception {
        super.start();

        // Services
        final JdbcRepositoryWrapper jdbc = JdbcRepositoryWrapper.getInstance(vertx);

        this.userServiceCRUD = new UserServiceCRUDImpl(jdbc);
        this.rentServiceCRUD = new RentServiceCRUDImpl(jdbc);

        this.userService = new UserServiceImpl(this.userServiceCRUD);
        this.rentService = new RentServiceImpl(this.rentServiceCRUD, this.userService);

        Router router = Router.router(vertx);

        router.route().handler(BodyHandler.create());

        //routes
        this.loadRoute(router);

        //deploy
        this.deployRestVerticle(router);

        //cors support
        this.enableCorsSupport(router);

        //http server
        this.createHttpServer(router, SystemUtil.getEnvironmentIntValue(AppEnum.APP_PORT.name()));
    }

    private void loadRoute(Router router) {
        //healthcheck
        router.get(API_HEALTHCHECK).handler(this::apiHealthCheck);
    }

    private void deployRestVerticle(Router router) {

        vertx.deployVerticle(new UserVerticle(router, this.userService, this.userServiceCRUD));
        vertx.deployVerticle(new RentVerticle(router, this.rentService));
    }

    private void apiHealthCheck(RoutingContext context) {
        context.response().setStatusCode(200).setStatusMessage("OK").end();
    }
}
