package cl.online.parking.verticles.api;

import cl.online.parking.model.commons.Pagination;
import cl.online.parking.service.business.RentService;
import cl.online.parking.verticles.ApiMainVerticle;
import cl.online.parking.verticles.RestAPIVerticle;
import io.vertx.core.json.Json;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class RentVerticle extends RestAPIVerticle {

    private static final String API_RENT = ApiMainVerticle.API_ONLINE.concat("/v1/rents");
    private static final String API_RENT_SEARCH = API_RENT.concat("/_search");
    private static final String API_RENT_BY_ID = API_RENT.concat("/rent/:id");

    private static final String ID = "id";
    private RentService rentService;

    public RentVerticle(Router router, RentService rentService) {
        this.loadRoute(router);

        this.rentService = rentService;
    }

    private void loadRoute(Router router) {
        router.post(API_RENT_SEARCH).handler(this::apiRentSearch);
        router.get(API_RENT_BY_ID).handler(this::apiRentById);
    }

    private void apiRentSearch(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            Pagination pagination = Json.decodeValue(context.getBodyAsString(), Pagination.class);

            this.rentService.searchRent(pagination).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context, Json::encodePrettily));
    }

    private void apiRentById(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            Integer rentId = Integer.valueOf(context.request().getParam(ID));

            this.rentService.getRentById(rentId).setHandler(resultHandler(context, Json::encodePrettily));

        }, false, resultHandler(context, Json::encodePrettily));
    }
}
