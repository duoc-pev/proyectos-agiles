package cl.online.parking.verticles.api;

import cl.online.parking.model.Estacionamiento;
import cl.online.parking.model.Usuario;
import cl.online.parking.model.commons.Pagination;
import cl.online.parking.service.business.UserService;
import cl.online.parking.service.crud.UserServiceCRUD;
import cl.online.parking.verticles.ApiMainVerticle;
import cl.online.parking.verticles.RestAPIVerticle;
import io.vertx.core.json.Json;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class UserVerticle extends RestAPIVerticle {

    private static final String API_USERS = ApiMainVerticle.API_ONLINE.concat("/v1/users");
    private static final String API_USER_SEARCH = API_USERS.concat("/_search");
    private static final String API_USER = API_USERS.concat("/user");
    private static final String API_USER_BY_ID = API_USER.concat("/:id");

    private static final String API_PARKING = ApiMainVerticle.API_ONLINE.concat("/v1/parking");
    private static final String API_PARKING_SEARCH = API_PARKING.concat("/_search");
    private static final String API_PARKING_BY_OWNER_ID = API_PARKING.concat("/:id");

    private static final String ID = "id";
    private UserService userService;
    private UserServiceCRUD userServiceCRUD;

    public UserVerticle(Router router, UserService userService, UserServiceCRUD userServiceCRUD) {
        this.loadRoute(router);

        this.userService = userService;
        this.userServiceCRUD = userServiceCRUD;
    }

    private void loadRoute(Router router) {
        router.post(API_USER_SEARCH).handler(this::apiUserSearch);
        router.post(API_USER).handler(this::apiNewUser);
        router.get(API_USER_BY_ID).handler(this::apiUserById);
        router.put(API_USER_BY_ID).handler(this::apiUpdateUser);
        router.post(API_PARKING_SEARCH).handler(this::apiSearchParking);
        router.get(API_PARKING_BY_OWNER_ID).handler(this::apiGetParkingByOwnerId);
        router.post(API_PARKING).handler(this::apiNewParking);
        router.put(API_PARKING_BY_OWNER_ID).handler(this::apiUpdateParking);
    }

    private void apiUserSearch(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            Pagination pagination = Json.decodeValue(context.getBodyAsString(), Pagination.class);

            this.userService.searchUsers(pagination).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context, Json::encodePrettily));
    }

    private void apiUserById(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            Integer userId = Integer.valueOf(context.request().getParam(ID));

            this.userService.getUserById(userId).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context, Json::encodePrettily));
    }

    private void apiSearchParking(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            Pagination pagination = Json.decodeValue(context.getBodyAsString(), Pagination.class);

            this.userService.searchParking(pagination, null).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context, Json::encodePrettily));
    }

    private void apiGetParkingByOwnerId(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            Integer ownerId = Integer.valueOf(context.request().getParam(ID));

            this.userService.searchParking(null, ownerId).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context, Json::encodePrettily));
    }

    private void apiNewUser(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            Usuario usuario = Json.decodeValue(context.getBodyAsString(), Usuario.class);

            this.userServiceCRUD.insertNewUser(usuario).setHandler(resultHandler(context));
        }, false, resultHandler(context));
    }

    private void apiUpdateUser(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            Usuario usuario = Json.decodeValue(context.getBodyAsString(), Usuario.class);
            usuario.setId(Integer.valueOf(context.request().getParam(ID)));

            this.userServiceCRUD.updateUser(usuario).setHandler(resultHandler(context));
        }, false, resultHandler(context));
    }

    private void apiNewParking(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            Estacionamiento estacionamiento = Json.decodeValue(context.getBodyAsString(), Estacionamiento.class);

            this.userServiceCRUD.insertParking(estacionamiento).setHandler(resultHandler(context));
        }, false, resultHandler(context));
    }


    private void apiUpdateParking(RoutingContext context)
    {
        context.vertx().executeBlocking(future-> {
            Estacionamiento estacionamiento = Json.decodeValue(context.getBodyAsString(), Estacionamiento.class);
            estacionamiento.setId(Integer.valueOf(context.request().getParam(ID)));

            this.userServiceCRUD.updateParking(estacionamiento).setHandler(resultHandler(context));
        }, false, resultHandler(context));
    }
}
