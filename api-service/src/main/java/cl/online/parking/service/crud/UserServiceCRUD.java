package cl.online.parking.service.crud;

import cl.online.parking.model.Estacionamiento;
import cl.online.parking.model.Usuario;
import cl.online.parking.model.direccion.Ciudad;
import cl.online.parking.model.direccion.Comuna;
import cl.online.parking.model.direccion.Provincia;
import cl.online.parking.model.permisos.Rol;
import io.vertx.core.Future;

import java.util.List;

public interface UserServiceCRUD {

    Future<List<Usuario>> searchUsers();

    Future<Usuario> getUserById(Integer id);

    Future<List<Rol>> searchRoles();

    Future<Rol> getRoleById(Integer roleId);

    Future<List<Estacionamiento>> searchParking();

    Future<List<Estacionamiento>> getParkingsByOwnerId(Integer ownerId);

    Future<List<Provincia>> getAllProvinces();

    Future<List<Ciudad>> getAllCities();

    Future<List<Comuna>> getAllCommunes();

    Future<Void> insertNewUser(Usuario usuario);

    Future<Void> updateUser(Usuario usuario);

    Future<Void> insertParking(Estacionamiento estacionamiento);

    Future<Void> updateParking(Estacionamiento estacionamiento);
}
