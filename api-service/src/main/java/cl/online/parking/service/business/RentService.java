package cl.online.parking.service.business;

import cl.online.parking.model.commons.Pagination;
import cl.online.parking.model.pagos.Arriendo;
import cl.online.parking.model.pagos.ArriendoResponse;
import io.vertx.core.Future;

public interface RentService {

    Future<ArriendoResponse> searchRent(Pagination pagination);

    Future<Arriendo> getRentById(Integer rentId);
}
