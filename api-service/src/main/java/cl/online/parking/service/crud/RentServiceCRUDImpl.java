package cl.online.parking.service.crud;

import cl.online.parking.dao.JdbcRepositoryWrapper;
import cl.online.parking.dao.RentDAO;
import cl.online.parking.model.pagos.Arriendo;
import io.vertx.core.Future;

import java.util.List;

public class RentServiceCRUDImpl implements RentServiceCRUD {

    private final RentDAO rentDAO;

    public RentServiceCRUDImpl(final JdbcRepositoryWrapper jdbc) {
        this.rentDAO = RentDAO.getInstance(jdbc);
    }

    @Override
    public Future<List<Arriendo>> searchRent() {
        return this.rentDAO.searchRent();
    }

    @Override
    public Future<Arriendo> getRentById(Integer rentId) {
        return this.rentDAO.getRentById(rentId);
    }
}
