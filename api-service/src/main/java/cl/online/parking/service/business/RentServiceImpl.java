package cl.online.parking.service.business;

import cl.online.parking.model.EstacionamientoResponse;
import cl.online.parking.model.Usuario;
import cl.online.parking.model.UsuarioResponse;
import cl.online.parking.model.commons.Count;
import cl.online.parking.model.commons.Pagination;
import cl.online.parking.model.pagos.Arriendo;
import cl.online.parking.model.pagos.ArriendoResponse;
import cl.online.parking.service.crud.RentServiceCRUD;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class RentServiceImpl implements RentService {

    private final RentServiceCRUD rentServiceCRUD;
    private final UserService userService;

    public RentServiceImpl(final RentServiceCRUD rentServiceCRUD, UserService userService) {
        this.rentServiceCRUD = rentServiceCRUD;
        this.userService = userService;
    }

    @Override
    public Future<ArriendoResponse> searchRent(Pagination pagination) {
        Future<ArriendoResponse> future = Future.future();
        ArriendoResponse response = new ArriendoResponse();

        Future<List<Arriendo>> rentFuture = Future.future();
        Future<UsuarioResponse> usuarioFuture = Future.future();
        Future<EstacionamientoResponse> parkingFuture = Future.future();

        this.rentServiceCRUD.searchRent().setHandler(rentFuture.completer());
        this.userService.searchUsers(null).setHandler(usuarioFuture.completer());
        this.userService.searchParking(null, null).setHandler(parkingFuture.completer());

        CompositeFuture.all(rentFuture, usuarioFuture, parkingFuture).compose(event -> {
            rentFuture.result().forEach(arriendo -> {
                arriendo.setArrendador(usuarioFuture.result().getUsuarios().stream().filter(u -> u.getId().equals(arriendo.getArrendador().getId())).findFirst().orElse(null));
                arriendo.setArrendatario(usuarioFuture.result().getUsuarios().stream().filter(u -> u.getId().equals(arriendo.getArrendatario().getId())).findFirst().orElse(null));
                arriendo.setEstacionamiento(parkingFuture.result().getEstacionamientos().stream().filter(e -> e.getId().equals(arriendo.getEstacionamiento().getId())).findFirst().orElse(null));

            });

            List<Arriendo> filteredList = rentFuture.result().stream().filter(arriendo -> Objects.nonNull(arriendo.getArrendatario()) && Objects.nonNull(arriendo.getArrendador())).collect(Collectors.toList());

            response.setCount(new Count(filteredList.size()));

            if (null != pagination) {
                filteredList = filteredList.stream().skip(pagination.getOffset()).limit(pagination.getLimit()).collect(Collectors.toList());
            }

            response.setArriendos(filteredList);

            future.complete(response);
        }, future);

        return future;
    }

    @Override
    public Future<Arriendo> getRentById(Integer rentId) {
        Future<Arriendo> future = Future.future();
        Future<Usuario> arrendadorFuture = Future.future();
        Future<Usuario> arrendatarioFuture = Future.future();
        Future<EstacionamientoResponse> estacionamientoFuture = Future.future();

        this.rentServiceCRUD.getRentById(rentId).compose(rent -> {
            this.userService.getUserById(rent.getArrendador().getId()).setHandler(arrendadorFuture.completer());
            this.userService.getUserById(rent.getArrendatario().getId()).setHandler(arrendatarioFuture.completer());
            this.userService.searchParking(null, rent.getEstacionamiento().getId()).setHandler(estacionamientoFuture.completer());

            CompositeFuture.all(arrendadorFuture, arrendatarioFuture, estacionamientoFuture).compose(event -> {
                rent.setEstacionamiento(estacionamientoFuture.result().getEstacionamientos().get(0));
                rent.setArrendatario(arrendatarioFuture.result());
                rent.setArrendador(arrendadorFuture.result());

                future.complete(rent);
            }, future);
        }, future);

        return future;
    }
}
