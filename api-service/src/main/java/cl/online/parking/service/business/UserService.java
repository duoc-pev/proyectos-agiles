package cl.online.parking.service.business;

import cl.online.parking.model.EstacionamientoResponse;
import cl.online.parking.model.Usuario;
import cl.online.parking.model.UsuarioResponse;
import cl.online.parking.model.commons.Pagination;
import io.vertx.core.Future;

public interface UserService {

    Future<UsuarioResponse> searchUsers(Pagination pagination);

    Future<Usuario> getUserById(Integer userId);

    Future<EstacionamientoResponse> searchParking(Pagination pagination, Integer ownerId);
}
