package cl.online.parking.service.crud;

import cl.online.parking.model.pagos.Arriendo;
import io.vertx.core.Future;

import java.util.List;

public interface RentServiceCRUD {

    Future<List<Arriendo>> searchRent();

    Future<Arriendo> getRentById(Integer rentId);
}
