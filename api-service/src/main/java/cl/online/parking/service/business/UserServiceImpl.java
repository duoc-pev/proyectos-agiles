package cl.online.parking.service.business;

import cl.online.parking.model.Estacionamiento;
import cl.online.parking.model.EstacionamientoResponse;
import cl.online.parking.model.Usuario;
import cl.online.parking.model.UsuarioResponse;
import cl.online.parking.model.commons.Count;
import cl.online.parking.model.commons.Pagination;
import cl.online.parking.model.direccion.Ciudad;
import cl.online.parking.model.direccion.Comuna;
import cl.online.parking.model.direccion.Provincia;
import cl.online.parking.model.permisos.Rol;
import cl.online.parking.service.crud.UserServiceCRUD;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class UserServiceImpl implements UserService {

    private final UserServiceCRUD userServiceCRUD;

    public UserServiceImpl(final UserServiceCRUD userServiceCRUD) {
        this.userServiceCRUD = userServiceCRUD;
    }

    @Override
    public Future<UsuarioResponse> searchUsers(Pagination pagination) {
        Future<UsuarioResponse> future = Future.future();

        UsuarioResponse response = new UsuarioResponse();

        Future<List<Usuario>> userListFuture = Future.future();
        Future<List<Rol>> roleListFuture = Future.future();

        this.userServiceCRUD.searchUsers().setHandler(userListFuture.completer());
        this.userServiceCRUD.searchRoles().setHandler(roleListFuture.completer());

        CompositeFuture.all(userListFuture, roleListFuture).compose(event -> {

            userListFuture.result().forEach(usuario -> {
                usuario.setRol(roleListFuture.result().stream().filter(rol -> rol.getId().equals(usuario.getRol().getId())).findFirst().orElse(null));
            });

            List<Usuario> filteredList = userListFuture.result().stream().filter(usuario -> Objects.nonNull(usuario.getRol())).collect(Collectors.toList());

            response.setCount(new Count(filteredList.size()));

            if (null != pagination) {
                filteredList = filteredList.stream().skip(pagination.getOffset()).limit(pagination.getLimit()).collect(Collectors.toList());
            }

            response.setUsuarios(filteredList);

            future.complete(response);
        }, future);

        return future;
    }

    @Override
    public Future<Usuario> getUserById(Integer userId) {
        Future<Usuario> future = Future.future();

        this.userServiceCRUD.getUserById(userId).compose(user -> {
            Future<Void> futureRole = Future.future();

            this.userServiceCRUD.getRoleById(user.getRol().getId()).compose(rol -> {
                user.setRol(rol);
                futureRole.complete();
                future.complete(user);
            }, futureRole);
        }, future);

        return future;
    }

    @Override
    public Future<EstacionamientoResponse> searchParking(Pagination pagination, Integer ownerId) {
        Future<EstacionamientoResponse> future = Future.future();
        EstacionamientoResponse response = new EstacionamientoResponse();

        Future<List<Estacionamiento>> estacionamientosFuture = Future.future();
        Future<List<Provincia>> provinciaFuture = Future.future();
        Future<List<Ciudad>> ciudadFuture = Future.future();
        Future<List<Comuna>> comunaFuture = Future.future();

        this.userServiceCRUD.getAllProvinces().setHandler(provinciaFuture.completer());
        this.userServiceCRUD.getAllCities().setHandler(ciudadFuture.completer());
        this.userServiceCRUD.getAllCommunes().setHandler(comunaFuture.completer());
        if (null != ownerId) {
            this.userServiceCRUD.getParkingsByOwnerId(ownerId).setHandler(estacionamientosFuture.completer());
        } else {
            this.userServiceCRUD.searchParking().setHandler(estacionamientosFuture.completer());
        }

        CompositeFuture.all(estacionamientosFuture, provinciaFuture, ciudadFuture, comunaFuture).compose(event -> {
            estacionamientosFuture.result().forEach(estacionamiento -> {
                estacionamiento.getDireccion().setComuna(comunaFuture.result().stream().filter(co -> co.getId().equals(estacionamiento.getDireccion().getComuna().getId())).findFirst().orElse(null));
                estacionamiento.getDireccion().setCiudad(ciudadFuture.result().stream().filter(c -> c.getId().equals(estacionamiento.getDireccion().getComuna().getParentId())).findFirst().orElse(null));
                estacionamiento.getDireccion().setProvincia(provinciaFuture.result().stream().filter(p -> p.getId().equals(estacionamiento.getDireccion().getCiudad().getParentId())).findFirst().orElse(null));
            });

            List<Estacionamiento> filteredList = estacionamientosFuture.result().stream().filter(e -> Objects.nonNull(e.getDireccion().getComuna()) && Objects.nonNull(e.getDireccion().getCiudad()) && Objects.nonNull(e.getDireccion().getProvincia())).collect(Collectors.toList());

            response.setCount(new Count(filteredList.size()));
            response.setEstacionamientos(filteredList);

            if (null != pagination) {
                response.setEstacionamientos(filteredList.stream().limit(pagination.getLimit()).skip(pagination.getOffset()).collect(Collectors.toList()));
            }

            future.complete(response);
        }, future);

        return future;
    }
}
