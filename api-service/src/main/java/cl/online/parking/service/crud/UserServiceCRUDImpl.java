package cl.online.parking.service.crud;

import cl.online.parking.dao.JdbcRepositoryWrapper;
import cl.online.parking.dao.UserDAO;
import cl.online.parking.model.Estacionamiento;
import cl.online.parking.model.Usuario;
import cl.online.parking.model.direccion.Ciudad;
import cl.online.parking.model.direccion.Comuna;
import cl.online.parking.model.direccion.Provincia;
import cl.online.parking.model.permisos.Rol;
import io.vertx.core.Future;

import java.util.List;

public class UserServiceCRUDImpl implements UserServiceCRUD {

    private final UserDAO userDAO;

    public UserServiceCRUDImpl(final JdbcRepositoryWrapper jdbc) {
        this.userDAO = UserDAO.getInstance(jdbc);
    }

    @Override
    public Future<List<Usuario>> searchUsers() {
        return this.userDAO.searchUsers();
    }

    @Override
    public Future<Usuario> getUserById(Integer userId) {
        return this.userDAO.getUserById(userId);
    }

    @Override
    public Future<List<Rol>> searchRoles() {
        return this.userDAO.searchRoles();
    }

    @Override
    public Future<Rol> getRoleById(Integer rolId) {
        return this.userDAO.getRoleById(rolId);
    }

    @Override
    public Future<List<Estacionamiento>> searchParking() {
        return this.userDAO.searchParking();
    }

    @Override
    public Future<List<Estacionamiento>> getParkingsByOwnerId(Integer ownerId) {
        return this.userDAO.getParkingsByOwnerId(ownerId);
    }

    @Override
    public Future<List<Provincia>> getAllProvinces() {
        return this.userDAO.getAllProvinces();
    }

    @Override
    public Future<List<Ciudad>> getAllCities() {
        return this.userDAO.getAllCities();
    }

    @Override
    public Future<List<Comuna>> getAllCommunes() {
        return this.userDAO.getAllCommunes();
    }

    @Override
    public Future<Void> insertNewUser(Usuario usuario) {
        return this.userDAO.insertNewUser(usuario);
    }

    @Override
    public Future<Void> updateUser(Usuario usuario) {
        return this.userDAO.updateUser(usuario);
    }

    @Override
    public Future<Void> insertParking(Estacionamiento estacionamiento) {
        return this.userDAO.insertParking(estacionamiento);
    }

    @Override
    public Future<Void> updateParking(Estacionamiento estacionamiento) {
        return this.userDAO.updateParking(estacionamiento);
    }
}
