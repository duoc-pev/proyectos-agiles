package cl.online.parking.dao;

import cl.online.parking.dao.query.QueryRentUtil;
import cl.online.parking.exception.AppException;
import cl.online.parking.model.pagos.Arriendo;
import io.vertx.core.Future;

import java.util.List;
import java.util.stream.Collectors;

public class RentDAO {

    public static RentDAO instance;
    private final JdbcRepositoryWrapper jdbc;

    private RentDAO(final JdbcRepositoryWrapper jdbc) {
        this.jdbc = jdbc;
    }

    public static synchronized RentDAO getInstance(final JdbcRepositoryWrapper jdbc) {
        if (null == instance) {
            instance = new RentDAO(jdbc);
        }

        return instance;
    }

    public Future<List<Arriendo>> searchRent() {
        Future<List<Arriendo>> future = Future.future();

        jdbc.retrieveAll(QueryRentUtil.searchRentSql(), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().stream().map(Arriendo::new).collect(Collectors.toList()));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<Arriendo> getRentById(Integer rentId) {
        Future<Arriendo> future = Future.future();

        jdbc.retrieveOne(rentId, QueryRentUtil.getRentByIdSql()).setHandler(optionalAsyncResult -> {
            if (optionalAsyncResult.succeeded()) {
                if (optionalAsyncResult.result().isPresent()) {
                    future.complete(new Arriendo(optionalAsyncResult.result().get()));
                } else {
                    future.fail(new AppException("No existe un arriendo para ese ID"));
                }
            } else {
                future.fail(optionalAsyncResult.cause());
            }
        });

        return future;
    }
}
