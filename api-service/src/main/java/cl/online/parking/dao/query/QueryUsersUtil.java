package cl.online.parking.dao.query;

import static cl.online.parking.util.Constant.DB_SCHEMA;
import static cl.online.parking.util.Constant.FROM;
import static cl.online.parking.util.Constant.LEFT_OUTER_JOIN;
import static cl.online.parking.util.Constant.SELECT;
import static cl.online.parking.util.Constant.WHERE;

public class QueryUsersUtil {

    private QueryUsersUtil() {
        throw new IllegalAccessError(QueryUsersUtil.class.toString());
    }

    public static String insertNewParkingSql() {
        return "insert into parking.estacionamiento (dueno_id, direccion_id, planta, identificacion) values (?, ?, ?, ?)";
    }

    public static String updateParkingSql() {
        return "update parking.estacionamiento set dueno_id = ?, direccion_id = ?, es_activo = ?, planta = ?, identificacion = ? where id = ?";
    }

    public static String insertNewUserSql() {

        return "insert into parking.usuarios (nombre, a_paterno, a_materno, rut_prefix, rut_dv, email, password, rol_id) values (?, ?, ? ,? ,? ,?, ?, ?)";
    }

    public static String updateUserSql() {
        return "update parking.usuarios set nombre = ?, a_paterno = ?, a_materno = ?, rut_prefix = ?, rut_dv = ?, email = ?, password = ?, rol_id = ? where id = ?";
    }

    public static String searchUsersSql() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectUserOnly());

        return query.toString();
    }

    public static String getUserByIdSql() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectUserOnly());
        query.append(WHERE);
        query.append(" u.id = ? ");

        return query.toString();
    }

    public static String searchRolesSql() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectRolesAndPermissions());

        return query.toString();
    }

    public static String getRoleByIdSql() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectRolesAndPermissions());
        query.append(WHERE);
        query.append(" r.id = ? ");

        return query.toString();
    }

    public static String searchParkingSql() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectParkingsWithAddress());

        return query.toString();
    }

    public static String getParkingsByOwnerIdSql() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectParkingsWithAddress());
        query.append(WHERE);
        query.append(" est.id = ? ");

        return query.toString();
    }

    public static String getAllProvincesSql() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(" p.id as prov_id, ");
        query.append(" p.nombre as prov_nombre, ");
        query.append(" p.romano as prov_romano, ");
        query.append(" p.orden as prov_orden ");
        query.append(FROM);
        query.append(DB_SCHEMA);
        query.append(".provincia p ");

        return query.toString();
    }

    public static String getAllCitiesSql() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(" c.id as ciudad_id, ");
        query.append(" c.nombre as ciudad_nombre, ");
        query.append(" c.prov_id as ciudad_parent_id ");
        query.append(FROM);
        query.append(DB_SCHEMA);
        query.append(".ciudad c ");

        return query.toString();
    }

    public static String getAllCommunes() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(" co.id as comuna_id, ");
        query.append(" co.nombre as comuna_nombre, ");
        query.append(" co.ciudad_id as comuna_parent_id ");
        query.append(FROM);
        query.append(DB_SCHEMA);
        query.append(".comuna co ");

        return query.toString();
    }

    private static StringBuilder selectUserOnly() {
        StringBuilder query = new StringBuilder();

        query.append(" u.id as user_id, ");
        query.append(" u.nombre as user_nombre, ");
        query.append(" u.a_paterno as user_a_paterno, ");
        query.append(" u.a_materno as user_a_materno, ");
        query.append(" u.rut_prefix, ");
        query.append(" u.rut_dv, ");
        query.append(" u.email as user_email, ");
        query.append(" u.rol_id ");
        query.append(FROM);
        query.append(DB_SCHEMA);
        query.append(".usuarios as u ");

        return query;
    }

    private static StringBuilder selectRolesAndPermissions() {
        StringBuilder query = new StringBuilder();

        query.append(" r.id as rol_id, ");
        query.append(" r.nombre as rol_nombre, ");
        query.append(" r.descripcion as rol_descripcion, ");
        query.append(" perm.id as perm_id, ");
        query.append(" perm.nombre as perm_nombre, ");
        query.append(" perm.descripcion as perm_descripcion ");
        query.append(FROM);
        query.append(DB_SCHEMA);
        query.append(".rol as r ");
        query.append(LEFT_OUTER_JOIN);
        query.append(" ");
        query.append(DB_SCHEMA);
        query.append(".roles_permisos as rp on (rp.rol_id = r.id) ");
        query.append(LEFT_OUTER_JOIN);
        query.append(" ");
        query.append(DB_SCHEMA);
        query.append(".permisos as perm on (perm.id = rp.permiso_id) ");

        return query;
    }

    private static StringBuilder selectParkingsWithAddress() {
        StringBuilder query = new StringBuilder();

        query.append(" est.id as est_id, ");
        query.append(" est.es_activo as est_es_activo, ");
        query.append(" est.planta as est_planta, ");
        query.append(" est.identificacion as est_identificacion, ");
        query.append(" u.id as user_id, ");
        query.append(" u.nombre as user_nombre, ");
        query.append(" u.a_paterno as user_a_paterno, ");
        query.append(" u.a_materno as user_a_materno, ");
        query.append(" u.rut_prefix, ");
        query.append(" u.rut_dv, ");
        query.append(" u.email as user_email, ");
        query.append(" u.rol_id, ");
        query.append(" ad.id as dir_id, ");
        query.append(" ad.com_id as comuna_id, ");
        query.append(" ad.linea_1 as dir_linea_1, ");
        query.append(" ad.linea_2 as dir_linea_2, ");
        query.append(" ad.cod_postal as dir_codigo_postal ");
        query.append(FROM);
        query.append(DB_SCHEMA);
        query.append(".estacionamiento as est ");
        query.append(LEFT_OUTER_JOIN);
        query.append(" ");
        query.append(DB_SCHEMA);
        query.append(".usuarios as u on (u.id = est.dueno_id) ");
        query.append(LEFT_OUTER_JOIN);
        query.append(" ");
        query.append(DB_SCHEMA);
        query.append(".direccion as ad on (ad.id = est.direccion_id) ");

        return query;

    }

}
