package cl.online.parking.dao;

import cl.online.parking.dao.query.QueryUsersUtil;
import cl.online.parking.exception.AppException;
import cl.online.parking.model.Estacionamiento;
import cl.online.parking.model.Usuario;
import cl.online.parking.model.direccion.Ciudad;
import cl.online.parking.model.direccion.Comuna;
import cl.online.parking.model.direccion.Provincia;
import cl.online.parking.model.permisos.Permiso;
import cl.online.parking.model.permisos.Rol;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class UserDAO {

    private static UserDAO instance;
    private final JdbcRepositoryWrapper jdbc;

    private UserDAO(final JdbcRepositoryWrapper jdbc) {
        this.jdbc = jdbc;
    }

    public static synchronized UserDAO getInstance(final  JdbcRepositoryWrapper jdbc) {
        if (null == instance) {
            instance = new UserDAO(jdbc);
        }

        return instance;
    }

    public Future<List<Usuario>> searchUsers() {
        Future<List<Usuario>> future = Future.future();

        jdbc.retrieveAll(QueryUsersUtil.searchUsersSql(), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().stream().map(Usuario::new).collect(Collectors.toList()));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<Usuario> getUserById(Integer userId) {
        Future<Usuario> future = Future.future();

        jdbc.retrieveOne(userId, QueryUsersUtil.getUserByIdSql()).setHandler(optionalAsyncResult -> {
            if (optionalAsyncResult.succeeded()) {
                if (optionalAsyncResult.result().isPresent()) {
                    future.complete(optionalAsyncResult.result().map(Usuario::new).orElse(new Usuario()));
                } else {
                    future.fail(new AppException("No existe un usuario con ese ID"));
                }
            } else {
                future.fail(optionalAsyncResult.cause());
            }
        });

        return future;
    }

    public Future<List<Rol>> searchRoles() {
        Future<List<Rol>> future = Future.future();

        jdbc.retrieveAll(QueryUsersUtil.searchRolesSql(), asyncResult -> {
            if (asyncResult.succeeded()) {
                Map<String, List<JsonObject>> mapPermission = asyncResult.result().stream().collect(Collectors.groupingBy(o -> o.getString("rol_nombre")));

                future.complete(mapPermission.entrySet().stream().map(stringListEntry -> new Rol(stringListEntry.getKey(), stringListEntry.getValue(), stringListEntry.getValue().stream().map(Permiso::new).collect(Collectors.toList()))).collect(Collectors.toList()));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<Rol> getRoleById(Integer roleId) {
        Future<Rol> future = Future.future();

        jdbc.retrieveMany(new JsonArray().add(roleId), QueryUsersUtil.getRoleByIdSql(), asyncResult -> {
             if (asyncResult.succeeded()) {
                 Map<String, List<JsonObject>> mapPermission = asyncResult.result().stream().collect(Collectors.groupingBy(o -> o.getString("rol_nombre")));

                 future.complete(mapPermission.entrySet().stream().map(stringListEntry -> new Rol(stringListEntry.getKey(), stringListEntry.getValue(), stringListEntry.getValue().stream().map(Permiso::new).collect(Collectors.toList()))).collect(Collectors.toList()).get(0));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<Void> insertParking(Estacionamiento estacionamiento){
        Future<Void> future = Future.future();

        jdbc.executeNoResult(estacionamiento.covertInsert(), QueryUsersUtil.insertNewParkingSql(), future.completer());

        return future;

    }

    public Future<Void> updateParking(Estacionamiento estacionamiento) {
        Future<Void> future = Future.future();

        jdbc.executeNoResult(estacionamiento.covertUpdate(), QueryUsersUtil.updateParkingSql(), future.completer());

        return future;
    }

    public Future<List<Estacionamiento>> searchParking() {
        Future<List<Estacionamiento>> future = Future.future();

        jdbc.retrieveAll(QueryUsersUtil.searchParkingSql(), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().stream().map(Estacionamiento::new).collect(Collectors.toList()));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<List<Estacionamiento>> getParkingsByOwnerId(Integer ownerId) {
        Future<List<Estacionamiento>> future = Future.future();

        jdbc.retrieveMany(new JsonArray().add(ownerId), QueryUsersUtil.getParkingsByOwnerIdSql(), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().stream().map(Estacionamiento::new).collect(Collectors.toList()));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<List<Provincia>> getAllProvinces() {
        Future<List<Provincia>> future = Future.future();

        jdbc.retrieveAll(QueryUsersUtil.getAllProvincesSql(), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().stream().map(Provincia::new).collect(Collectors.toList()));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<List<Ciudad>> getAllCities() {
        Future<List<Ciudad>> future = Future.future();

        jdbc.retrieveAll(QueryUsersUtil.getAllCitiesSql(), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().stream().map(Ciudad::new).collect(Collectors.toList()));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<List<Comuna>> getAllCommunes() {
        Future<List<Comuna>> future = Future.future();

        jdbc.retrieveAll(QueryUsersUtil.getAllCommunes(), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().stream().map(Comuna::new).collect(Collectors.toList()));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<Void> insertNewUser(Usuario usuario) {
        Future<Void> future = Future.future();

        jdbc.executeNoResult(usuario.convertInsert(), QueryUsersUtil.insertNewUserSql(), future.completer());

        return future;
    }

    public Future<Void> updateUser(Usuario usuario) {
        Future<Void> future = Future.future();

        jdbc.executeNoResult(usuario.convertUpdate(), QueryUsersUtil.updateUserSql(), future.completer());

        return future;
    }
}
