package cl.online.parking.dao;

import cl.online.parking.util.AppEnum;
import cl.online.parking.util.DataBaseEnum;
import cl.online.parking.util.SystemUtil;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.asyncsql.PostgreSQLClient;
import io.vertx.ext.sql.SQLClient;
import io.vertx.ext.sql.SQLConnection;

import java.util.List;
import java.util.Optional;

public class JdbcRepositoryWrapper {

    private static JdbcRepositoryWrapper instance;
    private final SQLClient client;

    private JdbcRepositoryWrapper(Vertx vertx) {
        client = PostgreSQLClient.createShared(vertx, this.config());
    }

    public static synchronized JdbcRepositoryWrapper getInstance(Vertx vertx) {
        if (instance == null) {
            instance = new JdbcRepositoryWrapper(vertx);
        }
        return instance;
    }

    public void executeNoResult(JsonArray params, String sql, Handler<AsyncResult<Void>> resultHandler) {

        client.getConnection(connHandler(resultHandler, connection -> connection.updateWithParams(sql, params, r -> {
            connection.close();
            if (r.succeeded()) {
                resultHandler.handle(Future.succeededFuture());
            } else {

                resultHandler.handle(Future.failedFuture(r.cause()));

            }
        })));
    }

    public void executeOneResult(JsonArray params, String sql, Handler<AsyncResult<Optional<JsonObject>>> resultHandler) {
        client.getConnection(connHandler(resultHandler, connection -> connection.queryWithParams(sql, params, r -> {
            connection.close();
            if (r.succeeded()) {
                List<JsonObject> resList = r.result().getRows();
                if (resList == null || resList.isEmpty()) {
                    resultHandler.handle(Future.succeededFuture(Optional.empty()));

                } else {
                    resultHandler.handle(Future.succeededFuture(Optional.of(resList.get(0))));
                }

            } else {
                resultHandler.handle(Future.failedFuture(r.cause()));
            }
        })));
    }

    public void retrieveOneResultWithoutParams(String sql, Handler<AsyncResult<JsonObject>> resultHandler) {
        client.getConnection(connHandler(resultHandler, connection -> connection.query(sql, r -> {
            connection.close();
            if(r.succeeded()) {
                JsonObject res = r.result().getRows().get(0);
                resultHandler.handle(Future.succeededFuture(res));
            } else {
                resultHandler.handle(Future.failedFuture(r.cause()));
            }
        })));
    }

    public void retrieveOneResultWithParams(JsonArray params, String sql, Handler<AsyncResult<JsonObject>> resultHandler) {
        client.getConnection(connHandler(resultHandler, connection -> connection.queryWithParams(sql, params, r -> {
            connection.close();
            if(r.succeeded()) {
                JsonObject res = r.result().getRows().get(0);
                resultHandler.handle(Future.succeededFuture(res));
            } else {
                resultHandler.handle(Future.failedFuture(r.cause()));
            }
        })));
    }

    public <K> Future<Optional<JsonObject>> retrieveOne(K param, String sql) {
        return getConnection()
                .compose(connection -> {
                    Future<Optional<JsonObject>> future = Future.future();
                    connection.queryWithParams(sql, new JsonArray().add(param), r -> {
                        connection.close();
                        if (r.succeeded()) {
                            List<JsonObject> resList = r.result().getRows();
                            if (resList == null || resList.isEmpty()) {
                                future.complete(Optional.empty());

                            } else {
                                future.complete(Optional.of(resList.get(0)));
                            }
                        } else {

                            future.fail(r.cause());
                        }
                    });
                    return future;
                });
    }

    public int calcPage(int page, int limit) {
        if (page <= 0)
            return 0;
        return limit * (page - 1);
    }

    public Future<List<JsonObject>> retrieveByPage(int page, int limit, String sql) {
        JsonArray params = new JsonArray().add(calcPage(page, limit)).add(limit);
        return getConnection().compose(connection -> {
            Future<List<JsonObject>> future = Future.future();
            connection.queryWithParams(sql, params, r -> {
                connection.close();
                if (r.succeeded()) {
                    future.complete(r.result().getRows());
                } else {

                    future.fail(r.cause());
                }
            });
            return future;
        });
    }

    public void retrieveMany(JsonArray params, String sql, Handler<AsyncResult<List<JsonObject>>> resultHandler) {
        client.getConnection(connHandler(resultHandler, connection -> connection.queryWithParams(sql, params, r -> {
            connection.close();
            if (r.succeeded()) {
                resultHandler.handle(Future.succeededFuture(r.result().getRows()));
            } else {
                resultHandler.handle(Future.failedFuture(r.cause()));
            }
        })));
    }

    public void retrieveAll(String sql, Handler<AsyncResult<List<JsonObject>>> resultHandler) {
        client.getConnection(connHandler(resultHandler, connection -> connection.query(sql, r -> {
            connection.close();
            if (r.succeeded()) {
                resultHandler.handle(Future.succeededFuture(r.result().getRows()));
            } else {
                resultHandler.handle(Future.failedFuture(r.cause()));
            }
        })));
    }

    public <K> void removeOne(K id, String sql, Handler<AsyncResult<Void>> resultHandler) {
        client.getConnection(connHandler(resultHandler, connection -> {
            JsonArray params = new JsonArray().add(id);
            connection.updateWithParams(sql, params, r -> {
                connection.close();
                if (r.succeeded()) {
                    resultHandler.handle(Future.succeededFuture());
                } else {
                    resultHandler.handle(Future.failedFuture(r.cause()));
                }

            });
        }));
    }

    public void removeAll(String sql, Handler<AsyncResult<Void>> resultHandler) {
        client.getConnection(connHandler(resultHandler, connection -> connection.update(sql, r -> {
            connection.close();
            if (r.succeeded()) {
                resultHandler.handle(Future.succeededFuture());
            } else {

                resultHandler.handle(Future.failedFuture(r.cause()));
            }
        })));
    }

    /**
     * A helper methods that generates async handler for SQLConnection
     *
     * @return generated handler
     */
    private <R> Handler<AsyncResult<SQLConnection>> connHandler(Handler<AsyncResult<R>> h1, Handler<SQLConnection> h2) {
        return conn -> {
            if (conn.succeeded()) {
                final SQLConnection connection = conn.result();
                h2.handle(connection);
            } else {
                h1.handle(Future.failedFuture(conn.cause()));
            }
        };
    }

    private Future<SQLConnection> getConnection() {
        Future<SQLConnection> future = Future.future();
        client.getConnection(future.completer());
        return future;
    }

    private JsonObject config() {
        final String username = SystemUtil.getEnvironmentStrValue(DataBaseEnum.DB_USER.name());
        final String password = SystemUtil.getEnvironmentStrValue(DataBaseEnum.DB_PASSWORD.name());
        final String database = SystemUtil.getEnvironmentStrValue(DataBaseEnum.DB_DATABASE.name());
        final int maxPoolSize = SystemUtil.getEnvironmentIntValue(DataBaseEnum.DB_POOL_SIZE.name());
        final int queryTimeout = SystemUtil.getEnvironmentIntValue(DataBaseEnum.DB_QUERY_TIMEOUT.name());
        final String databaseHost = SystemUtil.getEnvironmentStrValue(DataBaseEnum.DB_HOST.name());
        final int appPort = SystemUtil.getEnvironmentIntValue(AppEnum.APP_PORT.name());

        return new JsonObject()
                .put("http.port", appPort)
                .put("username", username)
                .put("password", password)
                .put("maxPoolSize", maxPoolSize)
                .put("database", database)
                .put("host", databaseHost)
                .put("queryTimeout", queryTimeout);

    }
}
