package cl.online.parking.dao.query;

import static cl.online.parking.util.Constant.DB_SCHEMA;
import static cl.online.parking.util.Constant.FROM;
import static cl.online.parking.util.Constant.SELECT;
import static cl.online.parking.util.Constant.WHERE;

public class QueryRentUtil {

    private QueryRentUtil() {
        throw new IllegalAccessError(QueryUsersUtil.class.toString());
    }

    public static String searchRentSql() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectRentOnly());

        return query.toString();
    }

    public static String getRentByIdSql() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectRentOnly());
        query.append(WHERE);
        query.append(" ar.id = ? ");

        return query.toString();
    }

    private static StringBuilder selectRentOnly() {
        StringBuilder query = new StringBuilder();

        query.append(" ar.id as arr_id, ");
        query.append(" ar.dueno_id as arr_arrendador_id, ");
        query.append(" ar.arrendatario_id as arr_arrendatario_id, ");
        query.append(" ar.est_id as arr_estacionamiento_id, ");
        query.append(" extract(epoch from ar.fecha_inicio) as arr_inicio, ");
        query.append(" extract(epoch from ar.fecha_termino) as arr_fin, ");
        query.append(" ar.valor_hora as arr_valor_hora, ");
        query.append(" round(DATE_PART('hour', ar.fecha_termino - ar.fecha_inicio ) * ar.valor_hora) as arr_valor_final ");
        query.append(FROM);
        query.append(DB_SCHEMA);
        query.append(".arriendo ar ");

        return query;
    }
}
