package cl.online.parking.model.commons;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Count {

    private Integer count;

    public Count(JsonObject json) {
        this.count = json.getInteger("count");
    }
    public Count (Integer count){
        this.count = count;
    }

}
