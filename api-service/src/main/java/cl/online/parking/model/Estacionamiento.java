package cl.online.parking.model;

import cl.online.parking.model.direccion.Direccion;
import cl.online.parking.util.JsonUtil;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Estacionamiento {

    private Integer id;
    private Usuario dueno;
    private Direccion direccion;
    private Boolean esActivo;
    private String planta;
    private String identificacion;

    public Estacionamiento(Integer id) {
        this.id = id;
    }

    public Estacionamiento(JsonObject json) {
        this.id = json.getInteger("est_id");
        this.dueno = new Usuario(json);
        this.direccion = new Direccion(json);
        this.esActivo = json.getBoolean("est_es_activo");
        this.planta = json.getString("est_planta");
        this.identificacion = json.getString("est_identificacion");
    }

    public JsonArray covertInsert(){
        JsonArray params = new JsonArray();

        JsonUtil.addParam(params, dueno.getId());
        JsonUtil.addParam(params, direccion.getId());
        JsonUtil.addParam(params, planta);
        JsonUtil.addParam(params, identificacion);

        return params;
    }

    public JsonArray covertUpdate(){
        JsonArray params = new JsonArray();

        JsonUtil.addParam(params, dueno.getId());
        JsonUtil.addParam(params, direccion.getId());
        JsonUtil.addParam(params, esActivo);
        JsonUtil.addParam(params, planta);
        JsonUtil.addParam(params, identificacion);
        JsonUtil.addParam(params, id);

        return params;
    }

}
