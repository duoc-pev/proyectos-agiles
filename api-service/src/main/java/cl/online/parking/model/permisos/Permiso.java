package cl.online.parking.model.permisos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Permiso {

    private Integer id;
    private String nombre;
    private String descripcion;

    public Permiso(JsonObject json) {
        this.id = json.getInteger("perm_id");
        this.nombre = json.getString("perm_nombre");
        this.descripcion = json.getString("perm_descripcion");
    }
}
