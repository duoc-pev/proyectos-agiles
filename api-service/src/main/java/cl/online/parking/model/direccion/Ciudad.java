package cl.online.parking.model.direccion;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Ciudad {

    private Integer id;
    private String nombre;
    private Integer parentId;

    public Ciudad(JsonObject json) {
        this.id = json.getInteger("ciudad_id");
        this.nombre = json.getString("ciudad_nombre");
        this.parentId = json.getInteger("ciudad_parent_id");
    }
}
