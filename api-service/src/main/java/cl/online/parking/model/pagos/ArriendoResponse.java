package cl.online.parking.model.pagos;

import cl.online.parking.model.commons.Count;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ArriendoResponse {

    private Count count;
    private List<Arriendo> arriendos;
}
