package cl.online.parking.model;

import cl.online.parking.util.FormatUtil;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Rut {
    private Integer rutPrefix;
    private String rutDv;
    private String rutFormateado;

    public Rut(JsonObject json) {
        this.rutPrefix = json.getInteger("rut_prefix");
        this.rutDv = json.getString("rut_dv");
        this.rutFormateado = FormatUtil.getFormattedRut(this.rutPrefix, this.rutDv);
    }
}
