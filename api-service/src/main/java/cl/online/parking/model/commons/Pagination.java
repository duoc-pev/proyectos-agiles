package cl.online.parking.model.commons;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Pagination {

    private Integer offset;
    private Integer limit;

    public Pagination(Integer offset, Integer limit){
        this.offset = offset;
        this.limit = limit;
    }
}
