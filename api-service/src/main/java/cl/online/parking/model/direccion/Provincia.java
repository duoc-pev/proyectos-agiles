package cl.online.parking.model.direccion;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Provincia {
    private Integer id;
    private String nombre;
    private String romano;
    private Integer orden;

    public Provincia(JsonObject json) {
        this.id = json.getInteger("prov_id");
        this.nombre = json.getString("prov_nombre");
        this.romano = json.getString("prov_romano");
        this.orden = json.getInteger("prov_orden");
    }

    public JsonArray convert() {
        JsonArray params = new JsonArray();

        params.add(this.id);
        params.add(this.nombre);

        return params;
    }
}
