package cl.online.parking.model.pagos;

import cl.online.parking.util.FormatUtil;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class TarjetaCredito {

    private Integer id;
    transient Long numero;
    private String numeroSalida;
    transient Integer cvv;
    private String mesVencimiento;
    private String anoVencimiento;
    private Boolean esActiva;

    public TarjetaCredito(Integer id) {
        this.id = id;
    }

    public TarjetaCredito(JsonObject json) {
        this.id = json.getInteger("tar_id");
        this.numero = json.getLong("tar_numero");
        this.numeroSalida = FormatUtil.hideCreditCardNumbers(8, json.getLong("tar_numero"));
        this.cvv = json.getInteger("tar_cvv");
        this.mesVencimiento = json.getString("tar_mes_vencimiento");
        this.anoVencimiento = json.getString("tar_ano_vecimiento");
        this.esActiva = json.getBoolean("tar_es_activa");
    }
}
