package cl.online.parking.model.pagos;

import cl.online.parking.model.Estacionamiento;
import cl.online.parking.model.Usuario;
import cl.online.parking.model.commons.TimeInstant;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Arriendo {

    private Integer id;
    private Usuario arrendador;
    private Usuario arrendatario;
    private Estacionamiento estacionamiento;
    private TimeInstant inicioArriendo;
    private TimeInstant finArriendo;
    private Integer valorHora;
    private Double valorFinal;

    public Arriendo(JsonObject json) {
        this.id = json.getInteger("arr_id");
        this.arrendador = new Usuario(json.getInteger("arr_arrendador_id"));
        this.arrendatario = new Usuario(json.getInteger("arr_arrendatario_id"));
        this.estacionamiento = new Estacionamiento(json.getInteger("arr_estacionamiento_id"));
        this.inicioArriendo = new TimeInstant(json.getLong("arr_inicio"));
        this.finArriendo = new TimeInstant(json.getLong("arr_fin"));
        this.valorHora = json.getInteger("arr_valor_hora");
        this.valorFinal = json.getDouble("arr_valor_final");
    }
}
