package cl.online.parking.model;

import cl.online.parking.model.commons.Count;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class UsuarioResponse {

    private Count count;
    private List<Usuario> usuarios;
}
