package cl.online.parking.model;

import cl.online.parking.model.permisos.Rol;
import cl.online.parking.util.JsonUtil;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Usuario {

    private Integer id;
    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private Rut rut;
    private String email;
    private Rol rol;
    private String password;

    public Usuario(Integer id) {
        this.id = id;
    }

    public Usuario(JsonObject json) {
        this.id = json.getInteger("user_id");
        this.nombre = json.getString("user_nombre");
        this.apellidoPaterno = json.getString("user_a_paterno");
        this.apellidoMaterno = json.getString("user_a_materno");
        this.rut = new Rut(json);
        this.email = json.getString("user_email");
        this.rol = new Rol(json);
    }

    public JsonArray convertInsert() {
        JsonArray params = new JsonArray();

        JsonUtil.addParam(params, nombre);
        JsonUtil.addParam(params, apellidoPaterno);
        JsonUtil.addParam(params, apellidoMaterno);
        JsonUtil.addParam(params, getRut().getRutPrefix());
        JsonUtil.addParam(params, getRut().getRutDv());
        JsonUtil.addParam(params, email);
        JsonUtil.addParam(params, password);
        JsonUtil.addParam(params, getRol().getId());

        return params;
    }

    public JsonArray convertUpdate() {
        JsonArray params = convertInsert();

        JsonUtil.addParam(params, id);

        return params;
    }
}
