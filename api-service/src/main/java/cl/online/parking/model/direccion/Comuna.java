package cl.online.parking.model.direccion;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Comuna {

    private Integer id;
    private String nombre;
    private Integer parentId;

    public Comuna(JsonObject json) {
        this.id = json.getInteger("comuna_id");
        this.nombre = json.getString("comuna_nombre");
        this.parentId = json.getInteger("comuna_parent_id");
    }
}
