package cl.online.parking.model.permisos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Rol {

    private Integer id;
    private String nombre;
    private String descripcion;
    private List<Permiso> permisos;

    public Rol(String nombre, List<JsonObject> jsonObjects, List<Permiso> permisos) {
        this.id = jsonObjects.get(0).getInteger("rol_id");
        this.nombre = nombre;
        this.descripcion = jsonObjects.get(0).getString("rol_descripcion");
        this.permisos = permisos;
    }

    public Rol(JsonObject json) {
        this.id = json.getInteger("rol_id");
        this.nombre = json.getString("rol_nombre");
        this.descripcion = json.getString("rol_descripcion");
        this.permisos = new ArrayList<>();
    }
}
