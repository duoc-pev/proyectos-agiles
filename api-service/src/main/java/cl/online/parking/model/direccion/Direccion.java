package cl.online.parking.model.direccion;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Direccion {

    private Integer id;
    private Provincia provincia;
    private Ciudad ciudad;
    private Comuna comuna;
    private String linea1;
    private String linea2;
    private Long codigoPostal;

    public Direccion(JsonObject json) {
        this.id = json.getInteger("dir_id");
        this.provincia = new Provincia(json);
        this.ciudad = new Ciudad(json);
        this.comuna = new Comuna(json);
        this.linea1 = json.getString("dir_linea_1");
        this.linea2 = json.getString("dir_linea_2");
        this.codigoPostal = json.getLong("dir_codigo_postal");
    }
}
