create or replace function generarFalsosMediosPagos() returns void as $$

    declare
        iterator integer := 1;
    begin
        while iterator < 1001
            loop
            insert into medio_pago (user_id, tarjeta_id) values (iterator, iterator);

        end loop;
    end;
$$ language plpgsql;

select generarFalsosMediosPagos();
