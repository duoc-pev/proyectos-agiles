
CREATE SEQUENCE parking.permisos_id_seq;

CREATE TABLE parking.permisos (
                id INTEGER NOT NULL DEFAULT nextval('parking.permisos_id_seq'),
                nombre VARCHAR(40) NOT NULL,
                descripcion VARCHAR(255),
                CONSTRAINT permisos_pk PRIMARY KEY (id)
);


ALTER SEQUENCE parking.permisos_id_seq OWNED BY parking.permisos.id;

CREATE SEQUENCE parking.rol_id_seq;

CREATE TABLE parking.rol (
                id INTEGER NOT NULL DEFAULT nextval('parking.rol_id_seq'),
                nombre VARCHAR(60) NOT NULL,
                descripcion VARCHAR(255),
                CONSTRAINT rol_pk PRIMARY KEY (id)
);


ALTER SEQUENCE parking.rol_id_seq OWNED BY parking.rol.id;

CREATE TABLE parking.roles_permisos (
                permiso_id INTEGER NOT NULL,
                rol_id INTEGER NOT NULL,
                CONSTRAINT roles_permisos_pk PRIMARY KEY (permiso_id, rol_id)
);


CREATE SEQUENCE parking.tarjetas_id_seq;

CREATE TABLE parking.tarjetas (
                id INTEGER NOT NULL DEFAULT nextval('parking.tarjetas_id_seq'),
                numero NUMERIC(30) NOT NULL,
                cvv NUMERIC(3) NOT NULL,
                mes_vencimiento NUMERIC(2) NOT NULL,
                ano_vencimiento NUMERIC(4) NOT NULL,
                es_activa BOOLEAN DEFAULT TRUE NOT NULL,
                CONSTRAINT tarjetas_pk PRIMARY KEY (id)
);


ALTER SEQUENCE parking.tarjetas_id_seq OWNED BY parking.tarjetas.id;

CREATE SEQUENCE parking.bancos_id_seq;

CREATE TABLE parking.bancos (
                id INTEGER NOT NULL DEFAULT nextval('parking.bancos_id_seq'),
                nombre VARCHAR(50) NOT NULL,
                es_activo BOOLEAN DEFAULT TRUE NOT NULL,
                CONSTRAINT bancos_pk PRIMARY KEY (id)
);


ALTER SEQUENCE parking.bancos_id_seq OWNED BY parking.bancos.id;

CREATE TABLE parking.cuenta_banco (
                id INTEGER NOT NULL,
                numero VARCHAR(20) NOT NULL,
                es_cuenta_corriente BOOLEAN DEFAULT TRUE NOT NULL,
                es_activa BOOLEAN DEFAULT TRUE NOT NULL,
                id_banco INTEGER NOT NULL,
                CONSTRAINT cuenta_banco_pk PRIMARY KEY (id)
);


CREATE SEQUENCE parking.provincia_id_seq;

CREATE TABLE parking.provincia (
                id INTEGER NOT NULL DEFAULT nextval('parking.provincia_id_seq'),
                nombre VARCHAR(50) NOT NULL,
                romano VARCHAR(4) NOT NULL,
                orden INTEGER NOT NULL,
                CONSTRAINT provincia_pk PRIMARY KEY (id)
);


ALTER SEQUENCE parking.provincia_id_seq OWNED BY parking.provincia.id;

CREATE SEQUENCE parking.ciudad_id_seq;

CREATE TABLE parking.ciudad (
                id INTEGER NOT NULL DEFAULT nextval('parking.ciudad_id_seq'),
                nombre VARCHAR(60) NOT NULL,
                prov_id INTEGER NOT NULL,
                CONSTRAINT ciudad_pk PRIMARY KEY (id)
);


ALTER SEQUENCE parking.ciudad_id_seq OWNED BY parking.ciudad.id;

CREATE SEQUENCE parking.comuna_id_seq;

CREATE TABLE parking.comuna (
                id INTEGER NOT NULL DEFAULT nextval('parking.comuna_id_seq'),
                nombre VARCHAR(60) NOT NULL,
                ciudad_id INTEGER NOT NULL,
                CONSTRAINT comuna_pk PRIMARY KEY (id)
);


ALTER SEQUENCE parking.comuna_id_seq OWNED BY parking.comuna.id;

CREATE SEQUENCE parking.direccion_id_seq;

CREATE TABLE parking.direccion (
                id INTEGER NOT NULL DEFAULT nextval('parking.direccion_id_seq'),
                com_id INTEGER NOT NULL,
                linea_1 VARCHAR(120) NOT NULL,
                linea_2 VARCHAR(120),
                cod_postal INTEGER NOT NULL,
                CONSTRAINT direccion_pk PRIMARY KEY (id)
);


ALTER SEQUENCE parking.direccion_id_seq OWNED BY parking.direccion.id;

CREATE SEQUENCE parking.usuarios_id_seq;

CREATE TABLE parking.usuarios (
                id INTEGER NOT NULL DEFAULT nextval('parking.usuarios_id_seq'),
                nombre VARCHAR(60) NOT NULL,
                a_paterno VARCHAR(60) NOT NULL,
                a_materno VARCHAR(60) NOT NULL,
                rut_prefix NUMERIC(8) NOT NULL,
                rut_dv VARCHAR(1) NOT NULL,
                email VARCHAR(90) NOT NULL,
		password VARCHAR(90) NOT NULL,
                rol_id INTEGER NOT NULL DEFAULT 2,
                CONSTRAINT usuarios_pk PRIMARY KEY (id)
);
COMMENT ON TABLE parking.usuarios IS 'Tabla que tendrá la información de los usuarios del sistema independiente de la categoria de este';


ALTER SEQUENCE parking.usuarios_id_seq OWNED BY parking.usuarios.id;

CREATE SEQUENCE parking.estacionamiento_id_seq;

CREATE TABLE parking.estacionamiento (
                id INTEGER NOT NULL DEFAULT nextval('parking.estacionamiento_id_seq'),
                dueno_id INTEGER NOT NULL,
                direccion_id INTEGER NOT NULL,
                es_activo BOOLEAN DEFAULT FALSE NOT NULL,
                planta VARCHAR(20) NOT NULL,
                identificacion VARCHAR(4) NOT NULL,
                CONSTRAINT estacionamiento_pk PRIMARY KEY (id, dueno_id, direccion_id)
);


ALTER SEQUENCE parking.estacionamiento_id_seq OWNED BY parking.estacionamiento.id;

CREATE SEQUENCE parking.arriendo_id_seq;

CREATE TABLE parking.arriendo (
                id INTEGER NOT NULL DEFAULT nextval('parking.arriendo_id_seq'),
                est_id INTEGER NOT NULL,
                dueno_id INTEGER NOT NULL,
                arrendatario_id INTEGER NOT NULL,
                fecha_inicio TIMESTAMP NOT NULL,
                fecha_termino TIMESTAMP NOT NULL,
                valor_hora INTEGER NOT NULL,
                direccion_id INTEGER NOT NULL,
                CONSTRAINT arriendo_pk PRIMARY KEY (id, est_id, dueno_id, arrendatario_id)
);


ALTER SEQUENCE parking.arriendo_id_seq OWNED BY parking.arriendo.id;

CREATE TABLE parking.medio_abono (
                id INTEGER NOT NULL,
                user_id INTEGER NOT NULL,
                cuenta_id INTEGER NOT NULL,
                CONSTRAINT medio_abono_pk PRIMARY KEY (id)
);


CREATE SEQUENCE parking.medio_pago_id_seq;

CREATE TABLE parking.medio_pago (
                id INTEGER NOT NULL DEFAULT nextval('parking.medio_pago_id_seq'),
                user_id INTEGER NOT NULL,
                tarjeta_id INTEGER NOT NULL,
                CONSTRAINT medio_pago_pk PRIMARY KEY (id)
);


ALTER SEQUENCE parking.medio_pago_id_seq OWNED BY parking.medio_pago.id;

CREATE SEQUENCE parking.transacciones_id_seq;

CREATE TABLE parking.transacciones (
                id INTEGER NOT NULL,
                est_id INTEGER NOT NULL,
                dueno_id INTEGER NOT NULL,
                arrendatario_id INTEGER NOT NULL,
                medio_pago_id INTEGER NOT NULL,
                fecha_trans TIMESTAMP NOT NULL,
                valor NUMERIC(5) NOT NULL,
                CONSTRAINT transacciones_pk PRIMARY KEY (id, est_id, dueno_id, arrendatario_id)
);

ALTER SEQUENCE parking.transacciones_id_seq OWNED BY parking.transacciones.id;


ALTER TABLE parking.roles_permisos ADD CONSTRAINT permisos_roles_permisos_fk
FOREIGN KEY (permiso_id)
REFERENCES parking.permisos (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE parking.usuarios ADD CONSTRAINT rol_usuarios_fk
FOREIGN KEY (rol_id)
REFERENCES parking.rol (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE parking.roles_permisos ADD CONSTRAINT rol_roles_permisos_fk
FOREIGN KEY (rol_id)
REFERENCES parking.rol (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE parking.medio_pago ADD CONSTRAINT tarjetas_medio_pago_fk
FOREIGN KEY (tarjeta_id)
REFERENCES parking.tarjetas (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE parking.cuenta_banco ADD CONSTRAINT bancos_cuenta_banco_fk
FOREIGN KEY (id_banco)
REFERENCES parking.bancos (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE parking.medio_abono ADD CONSTRAINT cuenta_banco_medio_abono_fk
FOREIGN KEY (cuenta_id)
REFERENCES parking.cuenta_banco (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE parking.ciudad ADD CONSTRAINT provincia_ciudad_fk
FOREIGN KEY (prov_id)
REFERENCES parking.provincia (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE parking.comuna ADD CONSTRAINT ciudad_comuna_fk
FOREIGN KEY (ciudad_id)
REFERENCES parking.ciudad (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE parking.direccion ADD CONSTRAINT comuna_direccion_fk
FOREIGN KEY (com_id)
REFERENCES parking.comuna (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE parking.estacionamiento ADD CONSTRAINT direccion_estacionamiento_fk
FOREIGN KEY (direccion_id)
REFERENCES parking.direccion (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE parking.medio_pago ADD CONSTRAINT usuarios_medio_pago_fk
FOREIGN KEY (user_id)
REFERENCES parking.usuarios (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE parking.medio_abono ADD CONSTRAINT usuarios_medio_abono_fk
FOREIGN KEY (user_id)
REFERENCES parking.usuarios (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE parking.estacionamiento ADD CONSTRAINT usuarios_estacionamiento_fk
FOREIGN KEY (dueno_id)
REFERENCES parking.usuarios (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE parking.arriendo ADD CONSTRAINT usuarios_arriendo_fk
FOREIGN KEY (arrendatario_id)
REFERENCES parking.usuarios (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE parking.arriendo ADD CONSTRAINT estacionamiento_arriendo_fk
FOREIGN KEY (est_id, dueno_id, direccion_id)
REFERENCES parking.estacionamiento (id, dueno_id, direccion_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE parking.transacciones ADD CONSTRAINT arriendo_transacciones_fk
FOREIGN KEY (dueno_id, est_id, id, arrendatario_id)
REFERENCES parking.arriendo (dueno_id, est_id, id, arrendatario_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE parking.transacciones ADD CONSTRAINT medio_pago_transacciones_fk
FOREIGN KEY (medio_pago_id)
REFERENCES parking.medio_pago (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;
